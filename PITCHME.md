#HSLIDE

## Swisscom goes agile

![Image](img/knife.jpg)

#HSLIDE

## Das agile Manifest

<span style="font-size:0.6em">
„Wir erschließen bessere Wege, Software zu entwickeln, indem wir es selbst tun
und anderen dabei helfen. Durch diese Tätigkeit haben wir diese Werte schätzen
gelernt:

* Menschen und Interaktionen stehen über Prozessen und Werkzeugen
* Funktionierende Software steht über einer umfassenden Dokumentation
* Zusammenarbeit mit dem Kunden steht über der Vertragsverhandlung
* Reagieren auf Veränderung steht über dem Befolgen eines Plans

Das heißt, obwohl wir die Werte auf der rechten Seite wichtig finden, schätzen
wir die Werte auf der linken Seite höher ein.“
</span>

#HSLIDE

##Warum

<span style="font-size:0.6em; color:gray">Wir wollen</span>
- eigenverantwortlich
- effizienter
- flexibler
- motivierter
- transparenter

mit Spaß als *Team* arbeiten

